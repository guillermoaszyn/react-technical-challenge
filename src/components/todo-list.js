import React from 'react';

const TodoList = ({ todoList, onAddTodo }) => {
  return (
    <div>
      <h1>Todo App</h1>
      <input label="todo" placeholder="Ej. Go to Supermarket" />
      <button label="submit">Add Todo</button>
      <ul>
        <li>Show the real todos in a list item</li>
      </ul>
    </div>
  );
};

export default TodoList;
