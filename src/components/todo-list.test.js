import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import TodoList from './todo-list';

describe('Components - TodoList', () => {
  let props;
  const renderComponent = (changedProps = {}) => {
    props = { ...props, ...changedProps };
    return render(<TodoList {...props} />);
  };

  beforeEach(() => {
    props = {};
    props.todoList = [];
    props.onAddTodo = jest.fn();
  });
  it('should list the todos', () => {
    const { getByText } = renderComponent({
      todoList: ['Wash the car', 'Walk the dog', 'Clean the kitchen'],
    });
    expect(getByText('Wash the car')).toBeInTheDocument();
    expect(getByText('Walk the dog')).toBeInTheDocument();
    expect(getByText('Clean the kitchen')).toBeInTheDocument();
  });
  it('should run the onAddTodo prop when clicking on the Add Todo Button with the todo written in the input', () => {
    const { getByRole } = renderComponent();
    fireEvent.change(getByRole('textbox'), {
      target: { value: 'Pay the rent' },
    });
    fireEvent.click(getByRole('button'));
    expect(props.onAddTodo).toHaveBeenCalledWith('Pay the rent');
  });
  it('should not let you add empty todos, and show a message letting you know', () => {
    const { getByRole, getByText } = renderComponent();
    fireEvent.click(getByRole('button'));
    expect(props.onAddTodo).not.toHaveBeenCalled();
    expect(getByText("Todo's can't be empty"));
  });
  it('should not let you add existing todos', () => {
    const { getByRole, getByText } = renderComponent({
      todoList: ['Wash the car', 'Walk the dog', 'Clean the kitchen'],
    });
    fireEvent.change(getByRole('textbox'), {
      target: { value: 'Clean the kitchen' },
    });
    fireEvent.click(getByRole('button'));
    expect(props.onAddTodo).not.toHaveBeenCalled();
    expect(getByText('This Todo already exists'));
  });
});
