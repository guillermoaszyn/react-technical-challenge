import { connect } from 'react-redux';
import TodoList from '../components/todo-list';
import { addTodo } from '../redux/actions';

const mapStateToProps = ({ todoList }) => ({
  todoList,
});

const mapDispatchToProps = { onAddTodo: addTodo };

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
