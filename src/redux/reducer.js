import * as types from './types';

export const initialState = {
  todoList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TODO:
      return {
        ...state,
        todoList: [...state.todoList, action.todo],
      };
    default:
      return state;
  }
};

export default reducer;
