import reducer, { initialState } from './reducer';
import * as actions from './actions';

describe('Redux - Reducer', () => {
  it('returns the initialState for unknown actions', () => {
    expect(reducer(undefined, { type: 'NOT_AN_ACTION' })).toEqual(initialState);
  });
  describe('addTodo', () => {
    it('adds a todo to the todoList', () => {
      expect(reducer(undefined, actions.addTodo('Pet the cat'))).toEqual({
        ...initialState,
        todoList: ['Pet the cat'],
      });
    });
  });
});
